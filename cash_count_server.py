import datetime

import pymongo
from pymongo import MongoClient
from flask import request, jsonify, Flask


app = Flask(__name__)

mongo_client = MongoClient('localhost', 27017)
db_cashcount = mongo_client['CashCount']
col_cashcount = db_cashcount['cc']


def handle_response(body, status_code):
    resp = jsonify(body)
    resp.status_code = status_code
    return resp


@app.route('/api/v1/cash_count', methods=['POST', 'GET'])
def manage_cash_count():
    sn = request.headers.get('sn')
    if request.method == 'GET':
        try:
            data = dict(col_cashcount.find_one({'sn': sn}, sort=[('_id', pymongo.DESCENDING)]))
            if data:
                data.pop('_id')
                return handle_response(body=data, status_code=200)
        except TypeError:
            pass
        return handle_response(body={}, status_code=404)

    elif request.method == 'POST':
        data = request.json
        data['sn'] = sn
        data['timestamp'] = datetime.datetime.now()
        col_cashcount.insert_one(data)
        return handle_response(body={'response': 'OK'}, status_code=201)


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=3000, load_dotenv=False)
