# CashCountServer

Cash Count Server

## Installation

- Install mongodb
    
        
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
        echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
        sudo apt update
        sudo apt install -y mongodb-org
        sudo systemctl start mongod
        sudo systemctl enable mongod

- Install python packages and start server
    
    
        sudo apt install python3-pip
        sudo pip3 install -U pip
        sudo pip3 install -r requirements.txt
        
- Configure nginx
    
        
        sudo apt install -y nginx
        sudo cp cash_count.nginx /etc/nginx/sites-available/cash_count
        sudo ln -s /etc/nginx/sites-available/cash_count /etc/nginx/sites-enabled/cash_count
        sudo service nginx restart
        
- Start server
    
        
        gunicorn wsgi -b 127.0.0.1:8000 --daemon -w4 --log-file=cash_count.log

